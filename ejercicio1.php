<?php
include_once('funciones.php');
if (!isset($argv[1])) {
	echo "Debe especificar el ejemplo a correr\n";
    die;
}else{
    $caso=$argv[1];
}
switch($caso){
    case '1':
        $camion=700;
        $vacas = [
            ['num' => 1, 'peso' => 360, 'leche' => 40],
            ['num' => 2, 'peso' => 250, 'leche' => 35],
            ['num' => 3, 'peso' => 400, 'leche' => 43],
            ['num' => 4, 'peso' => 180, 'leche' => 28],
            ['num' => 5, 'peso' => 50, 'leche' => 12],
            ['num' => 6, 'peso' => 90, 'leche' => 13],
        ];
        break;
    case '2':
        $camion=1000;
        $vacas = [
            ['num' => 1, 'peso' => 223, 'leche' => 30],
            ['num' => 2, 'peso' => 243, 'leche' => 34],
            ['num' => 3, 'peso' => 100, 'leche' => 28],
            ['num' => 4, 'peso' => 200, 'leche' => 45],
            ['num' => 5, 'peso' => 200, 'leche' => 31],
            ['num' => 6, 'peso' => 155, 'leche' => 50],
            ['num' => 7, 'peso' => 300, 'leche' => 29],
            ['num' => 8, 'peso' => 150, 'leche' => 1],
        ];
        break;
        case '3':
            $camion=2000;
            $vacas = [
                ['num' => 1, 'peso' => 340, 'leche' => 45],
                ['num' => 2, 'peso' => 355, 'leche' => 50],
                ['num' => 3, 'peso' => 223, 'leche' => 34],
                ['num' => 4, 'peso' => 243, 'leche' => 39],
                ['num' => 5, 'peso' => 130, 'leche' => 29],
                ['num' => 6, 'peso' => 240, 'leche' => 40],
                ['num' => 7, 'peso' => 260, 'leche' => 30],
                ['num' => 8, 'peso' => 155, 'leche' => 52],
                ['num' => 9, 'peso' => 302, 'leche' => 31],
                ['num' => 10, 'peso' => 130, 'leche' => 1],
            ];
            break;
}

$totalVacas = count($vacas);


$combinaciones = getCombinaciones($totalVacas);

$totalLitros=0;

foreach ($combinaciones as $c) {
    $peso=0;
    $litros=0;
    foreach($c as $data){
        $peso+=$vacas[$data-1]['peso'];
        $litros +=$vacas[$data-1]['leche'];
    }

    if($peso<=$camion && $litros>$totalLitros){
        $totalLitros=$litros;
        $seleccion = ['seleccion'=>implode(", ", $c),'litros'=>$totalLitros,'peso'=>$peso];
    }
}
print_r($seleccion);