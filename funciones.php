<?php
/**
 * funcion para agregar combinaciones al arreglo
 * @param array $listaActual  de combinaciones obtenidas
 * @param int $desde  desde donde deben empezar a combinar
 * @param int $hasta  hasta donde debe combinar
 * @return array de combinaciones
 */
function addCombinaciones($listaActual, $desde, $hasta) {
    $nvaLista = [];

    for ($i=$desde; $i <= $hasta; $i++) {
        $copy = $listaActual;
        $copy[] = $i;
        $nvaLista = array_merge($nvaLista, [$copy], addCombinaciones($copy, $i+1, $hasta));
    }

    return $nvaLista;
}

/**
 * funcion para obtener las combinaciones posibles segun el total de vacas
 * @param int $totalVacas  cantidad de vacas disponibles
 * @return array del total combinaciones posibles
 */
function getCombinaciones($totalVacas) {
    $comb = [];
    
    for ($i=1; $i<= $totalVacas; $i++) {
        $comb[] = [$i];
        $comb = array_merge($comb, addCombinaciones([$i], $i+1, $totalVacas));
    }

    return $comb;
}

/**
 * funcion que procesa las lineas leidas de un archivo para formatear los datos
 * @param array $data  de las lineas leidas
 * @return array con los datos formateados
 */
function procesaLinea($data){
    $seccion=0;
    $lineas=array_filter($data, 'strlen' );
    foreach($lineas as $linea){
        if(substr($linea, 0, 1)==='#'){
            $seccion++;
        }
        switch($seccion){
            case 1:
                $totalTareas=$linea;
                break;
            case 2:
                $timeTask=explode(',',$linea);
                $tiempoTarea[]=array('tarea'=>$timeTask[0],'tiempo'=>$timeTask[1]);
                break;
            case 3:
                $depTask=explode(',',$linea);
                $dependenciaTarea[]=array('tarea'=>array_shift($depTask),'dependencia'=>$depTask);
                break;
        }
    }
    return array('total'=>$totalTareas,'tiempo'=>$tiempoTarea,'dependencias'=>$dependenciaTarea);
}

/**
 * funcion que busca el tiempo de ejecucion de cada tarea y lo suma
 * @param array $data  con los datos de las lineas formateados
 * @param array $tasks  con las tareas y sus dependencias
 * @return int contentivo del tiempo total de ejecucion
 */
function calcularTiempo($data,$tasks){
    $tiempoTotal=0;
    foreach($tasks as $dat){
        $tiempo= array_values(array_filter($data['tiempo'], fn($n) => $n['tarea']==$dat));
        $tiempoTotal+=$tiempo[0]['tiempo'];
    }
    return $tiempoTotal;
}

/**
 * funcion para obtener todas las actividades involucradas en la ejecucion de otra actividad por sus dependencias
 * @param array $datos  con los datos de las lineas formateados
 * @param int $task  con la tare que se busca
 * @return array con las tareas involucradas segun la dependencia
 */
function calcularDependencias($datos,$task){
    $dependencias = array_filter($datos['dependencias'], fn($n) => $n['tarea']==$task);
    $dep2=array_values($dependencias);
    $arrayTest[]=$dep2[0]['dependencia'];
    if(gettype($dep2[0]['dependencia'])=='array'){
        foreach($dep2[0]['dependencia'] as $dat){
            $nvaDep= calcularDependencias($datos,$dat);
            array_push($arrayTest[0],$nvaDep[0][0]);
        }
    }else{
        $arrayTest[]=$dep2[0]['dependencia'];
    }
    return $arrayTest[0];
}