#Descripción

Acá se enceuntra la solución a los 2 ejercicios planteados. Es un desarrollo hecho en php puro.

1.-Para ejecutar la solución al ejercicio 1 debes correr el script indicando como parámetro el numero del ejemplo que quieres ver (1,2,3).

ejemplo:
```
php ejercicio1.php 2
```

2.-Para ejecutar la solución del ejercicio 2 debes correr el script indicando la actividad que quieres ver cuanto tarda como parámetro (0,1,2,3,4,5).

ejemplo:
```
php ejercicio2.php 3
```

El codigo contiene un archivo compartido llamado **"funciones.php"** por ambos ejercicios donde estan las funciones que se llaman desde cada uno.

Contiene un archivo txt llamado **"tareas.txt"** el cual es leido en el ejercico 2 para ejecutar su funcion.

###Para jecutar estos archivos necesitas como minimo php-7.4.

####Estos ejercicios estan diseñador para correlos por línea de comandos (CLI).

#### El repositorio de este proyecto es: <https://gitlab.com/jetrias/pjv>