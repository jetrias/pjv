<?php
include_once('funciones.php');
if (!isset($argv[1])) {
	echo "Debe especificar la tarea a evaluar\n";
    die;
}else{
    $taskP=$argv[1];
}

$archivo = fopen('tareas.txt','r');

while ($linea = fgets($archivo)) {
    $numlinea++;
    $aux[] = trim($linea);
}
fclose($archivo);

$tareas = procesaLinea($aux);

$dependencias = calcularDependencias($tareas, $taskP);
$cleaned=[];
if(gettype($dependencias)=='array'){
    $cleaned = array_filter($dependencias, 'strlen' );
}
array_push($cleaned,$taskP);
$tiempo=calcularTiempo($tareas, $cleaned);
echo $taskP.', '.$tiempo;